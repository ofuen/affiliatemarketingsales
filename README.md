# :globe_with_meridians: AffiliateMarketingSales
Affiliate Marketing Sales Page Template<br><br>
[![Github commit merge status](https://img.shields.io/github/commit-status/badges/shields/master/5d4ab86b1b5ddfb3c4a70a70bd19932c52603b8c.svg)](https://github.com/ofuen/AffiliateMarketingSales/tree/master)
[![GitHub contributors](https://img.shields.io/github/contributors/ofuen/AffiliateMarketingSales.svg)](https://github.com/ofuen/AffiliateMarketingSales/graphs/contributors/)
[![GitHub last commit](https://img.shields.io/github/last-commit/ofuen/AffiliateMarketingSales.svg)](https://github.com/ofuen/AffiliateMarketingSales/)
[![GitHub issues](https://img.shields.io/github/issues-raw/ofuen/AffiliateMarketingSales.svg)](https://github.com/ofuen/AffiliateMarketingSales/issues)
[![GitHub closed issues](https://img.shields.io/github/issues-closed-raw/ofuen/AffiliateMarketingSales.svg)](https://github.com/ofuen/AffiliateMarketingSales/issues?q=is%3Aissue+is%3Aclosed)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![made-for-VSCode](https://img.shields.io/badge/Made%20for-VSCode-1f425f.svg)](https://code.visualstudio.com/)
***
![image](https://user-images.githubusercontent.com/19554935/49329202-8bb34700-f549-11e8-9696-3d59e9ac4830.png)
![image](https://user-images.githubusercontent.com/19554935/49558937-e1d21280-f8da-11e8-8541-5f2a004a8f20.png)
![image](https://github.com/ofuen/AffiliateMarketingSales/blob/master/screenshot/sass-banner.jpg)

***
# Affiliate Marketing Sales Page Template

* :mega: [Introduction](#introduction)
* :page_facing_up: [License](#license)
* :pushpin: [Quick start for users](#quick-start-for-users)
* :file_folder: [Configuration](#configuration)
***
## Introduction 
 A customizable HTML template for affiliate marketers.

## License
The code is licensed under an MIT License.

See [the license file](LICENSE) for further details.

## Quick start for users
> **Note:**
>
> To clone the repo to the following
>```bash
> git clone https://github.com/ofuen/AffiliateMarketingSales.git
>```
> Download zip :bookmark: [AffiliateMarketingSales](https://github.com/ofuen/AffiliateMarketingSales/archive/master.zip)

## Configuration
> **Note:**
>
> Below are the Navigation and Table sass variables
> where to change the look and feel.
>
> ```sass
> _variables.scss
> // Navigation Variables
> $content-width: 1000px;
> $breakpoint: 799px;
> $nav-height: 70px;
> $nav-background: #339EFF;
> $nav-font-color: #ffffff;
> $link-hover-color: #2581DC;
> 
> // Table Variables
> $primary-color: #008cba;
> $gray-color: #ddd;
> $medium-up: 600px;
> $large-up: 1000px;
> $global-radius: 4px;
>
> ```
> 
> See [_mixins.scss](https://github.com/ofuen/AffiliateMarketingSales/blob/master/sass/_mixins.scss).
***

![Alt Text](https://github.com/ofuen/AffiliateMarketingSales/blob/master/screenshot/2018-12-15_22-33-35.gif)
***
[![forthebadge](https://forthebadge.com/images/badges/validated-html5.svg)](https://validator.w3.org/)
[![forthebadge](https://forthebadge.com/images/badges/uses-css.svg)](https://www.w3.org/Style/Examples/011/firstcss.en.html)
[![forthebadge](https://forthebadge.com/images/badges/winter-is-coming.svg)](https://media.giphy.com/media/MrxXXBriEIKBO/giphy.gif)
[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://media.giphy.com/media/l3vRabxJJKmSmlIAw/giphy.gif)
[![forthebadge](https://forthebadge.com/images/badges/uses-git.svg)](https://git-scm.com/downloads)
***
| 📄 Description | 🌐 Website |
| --- | --- |
| AFFILIATE MARKETPLACE | https://ofuen.github.io/AffiliateMarketingSales/ |
***
| Here is how you can compile .scss files using the node CLI:  | 
| ------------- | 

```sass
node-sass input.scss output.css
```
Template1 | Template2
------------------|-------------------
![Compression Ratio](screenshot/images.jpg "Compression Ratio") | ![Compression Speed](screenshot/images1.jpg "Compression Speed") | 
